# django-single-page
This is a very simple implementation of single-page functionality with django and javascript's fetch API.

# Documentation du Pipeline CI/CD

## Vue d'ensemble

Cette pipeline est conçue pour un projet Django utilisant l'image `python:3.9`. La pipeline se compose de trois étapes principales : `build`, `test` et `deploy`.

## Configuration

### Variables

- `PIP_CACHE_DIR` : Chemin vers le répertoire de cache de pip. Défini sur `$CI_PROJECT_DIR/.cache/pip`.

### Cache

Les chemins suivants sont mis en cache pour accélérer la pipeline :

- `.cache/pip` : Répertoire de cache de pip.
- `venv/` : Répertoire de l'environnement virtuel.

## Avant le script (Before Script)

Avant l'exécution des étapes, les commandes de configuration suivantes sont exécutées :

- La version de Python est affichée à des fins de débogage.
- Virtualenv est installé.
- Un nouvel environnement virtuel est créé et activé.
- Django est installé.

## Étapes

### 1. Build (Construction)

Dans cette étape, les dépendances du projet sont installées :

- Un environnement virtuel est créé à l'aide de `virtualenv`.
- Django est installé à l'aide de pip.
- Si un fichier `requirements.txt` n'existe pas, il est généré avec `pip freeze`.
- Les dépendances de `requirements.txt` sont installées.

### 2. Test

Dans l'étape de test :

- La commande de test intégrée de Django est exécutée avec `python manage.py test`.

### 3. Deploy (Déploiement)

Dans l'étape de déploiement :

- Indique le début de la phase de déploiement.

---
