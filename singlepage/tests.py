from django.test import TestCase

# Create your tests here.

from django.test import TestCase
from django.urls import reverse
from django.http import Http404
from .views import texts

class SimplePageTestCase(TestCase):
    def test_index_page(self):
        # Utilise la méthode client pour simuler une requête GET sur l'URL de la page d'index.
        response = self.client.get(reverse('index'))
        # Vérifie que la requête est bien réussie, donc le code de statut doit être 200.
        self.assertEqual(response.status_code, 200)
        # Vérifie que le template utilisé est le bon
        self.assertTemplateUsed(response, 'singlepage/index.html')

    def test_section_page_valid(self):
        for i, text in enumerate(texts, 1):
            response = self.client.get(reverse('section', args=[i]))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content.decode(), text)

    def test_section_page_invalid(self):
        response = self.client.get(reverse('section', args=[0]))
        self.assertEqual(response.status_code, 404)
        response = self.client.get(reverse('section', args=[4]))
        self.assertEqual(response.status_code, 404)